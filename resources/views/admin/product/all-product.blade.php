
@extends('admin.master')
@section('title')
    Product
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Products Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Product List
                <a href="{{route('admin.add-product')}}" class="btn btn-sm btn-warning" style="float: right;"
                >Add New</a>
            </h6>
            <br>
            <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-15p">Product Code</th>
                        <th class="wd-15p">Product Name</th>
                        <th class="wd-15p">Image</th>
                        <th class="wd-15p">Category</th>
                        <th class="wd-20p">Brand</th>
                        <th class="wd-20p">Quantity</th>
                        <th class="wd-20p">Status</th>
                        <th class="wd-20p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($products as $product)
                            <td>{{$product->product_code}}</td>
                            <td>{{$product->product_name}}</td>
                            <td>
                                <img src="{{URL::to($product->image_one)}}" height="60px" width="50px" alt="">
                            </td>
                            <td>{{$product->category_name}}</td>
                            <td>{{$product->brand_name}}</td>
                            <td>{{$product->product_quantity}}</td>
                            <td>
                                @if($product->status==1)
                                    <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-danger">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{URL::to('edit/product',$product->id)}}" class="btn btn-sm btn-info" title="Edit"> <i class="fas fa-edit"></i></a>
                                <a href="{{URL::to('delete/product',$product->id)}}" class="btn btn-sm btn-danger" title="Delete" id="delete"><i class="fas fa-trash-alt"></i></a>
                                <a href="{{URL::to('view/product',$product->id)}}" class="btn btn-sm btn-warning" title="view"><i class="fas fa-eye"></i></a>
                                @if($product->status==1)
                                    <a href="{{URL::to('inactive/product',$product->id)}}" class="btn btn-sm btn-danger" title="Inactive" ><i class="fas fa-thumbs-down"></i></a>
                                @else
                                    <a href="{{URL::to('active/product',$product->id)}}" class="btn btn-sm btn-success" title="active"><i class="fas fa-thumbs-up"></i></a>
                                @endif
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div><!-- card -->
        </div><!-- card -->
        @endsection
        @section('script')
            <script>
                $(function(){
                    'use strict';
                    $('#datatable1').DataTable({
                        responsive: true,
                        language: {
                            searchPlaceholder: 'Search...',
                            sSearch: '',
                            lengthMenu: '_MENU_ items/page',
                        }
                    });
                });
            </script>
@endsection

