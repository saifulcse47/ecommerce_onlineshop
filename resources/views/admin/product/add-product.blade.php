@extends('admin.master')
@section('title')
    Add Product
    @endsection
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" crossorigin="anonymous">
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Product Layouts</h5>
            <p>Forms are used to collect user information with different element types of input, select, checkboxes, radios and more.</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Add New Product</h6>
            <p class="mg-b-20 mg-sm-b-30">Star mark Filed must required</p>

            <div class="form-layout">
                    <form action="{{route('store.product')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row mg-b-25">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="product_name" value="" placeholder="Product Name">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Code: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="product_code" value="" placeholder="Product Code">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Quantity: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="product_quantity"  placeholder="Product Quantity">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Category: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" name="category_id" data-placeholder="Choose Category">
                                    <option>Choose Category</option>
                                    @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                         <div class="col-lg-4">
                                <div class="form-group mg-b-10-force">
                                    <label class="form-control-label">Sub Category: <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="sub_category_id" data-placeholder="Choose Subcategory">

                                    </select>
                                </div>
                          </div>
                            <div class="col-lg-4">
                                <div class="form-group mg-b-10-force">
                                    <label class="form-control-label">Brand: <span class="tx-danger">*</span></label>
                                    <select class="form-control select2" name="brand_id" data-placeholder="Choose Brand">
                                        <option label="Select Brand Name"></option>
                                        @foreach($brands as $brand)
                                        <option value="{{$brand->id}}">{{$brand->brand_name}} <img src="{{asset($brand->brand_logo)}}" height="30" width="30" alt=""></option>
                                         @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group mg-b-10-force">
                                    <label class="form-control-label">Product Size: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" id="size" data-role="tagsinput" name="product_size" placeholder="Product Size">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group mg-b-10-force">
                                    <label class="form-control-label">Product Color: <span class="tx-danger">*</span></label>
                                    <input class="form-control" data-role="tagsinput" id="color"type="text" id="size" data-role="tagsinput" name="product_colors" placeholder="Product Color">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group mg-b-10-force">
                                    <label class="form-control-label">Selling Price: <span class="tx-danger">*</span></label>
                                    <input class="form-control" type="text" id="size"  name="selling_price" placeholder="Product Selling Prize">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group mg-b-10-force">
                                    <label class="form-control-label">Product Details: <span class="tx-danger">*</span></label>
                                    <textarea class="form-control" id="summernote" name="product_details">

                                    </textarea>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group mg-b-10-force">
                                    <label class="form-control-label">Video Link: <span class="tx-danger">*</span></label>
                                    <input class="form-control"  name="video_link">
                                </div>
                            </div>
                            <!-- -------------col-4 image upload -------------- -->
                            <div class="col-lg-3">
                                <label class="form-control-label">Image One(Main thumbnail): <span class="tx-danger">*</span></label>
                                <label class="custom-file">
                                    <input type="file" id="file" name="image_one" class="custom-file-input" onchange="readURL(this);" required="" accept="image">
                                    <span class="custom-file-control"></span>
                                    <img src="#" id="one" alt="">
                                </label>
                            </div><!-- col -->
                            <div class="col-lg-3 mg-t-40 mg-lg-t-0">
                                <label class="form-control-label">ImageTwo: <span class="tx-danger">*</span></label>
                                <label class="custom-file">
                                    <input type="file" id="file2" name="image_two" class="custom-file-input" onchange="readURL1(this);" required="" accept="image">
                                    <img src="#" id="two" alt="">
                                    <span class="custom-file-control custom-file-control-primary"></span>
                                </label>
                            </div><!-- col -->
                            <div class="col-lg-3 mg-t-40 mg-lg-t-0">
                                <label class="form-control-label">Image Three: <span class="tx-danger">*</span></label>
                                <label class="custom-file">
                                    <input type="file" name="image_three" class="custom-file-input" onchange="readURL2(this);" required="" accept="image">
                                    <img src="#" id="three" alt="">
                                    <span class="custom-file-control custom-file-control-inverse"></span>
                                </label>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <hr>
                            <!-- -------------end image upload -------------- -->
                            <!-- -------------slider checkbox -------------- -->
                           <div class="row mg-t-40 mg-lg-t-0 ml-2" style="margin-top: 40px">
                               <div class="col-lg-4">
                                   <label class="ckbox">
                                       <input type="checkbox" name="main_slider" value="1">
                                       <span class="">Main Slider</span>
                                   </label>
                               </div>
                               <div class="col-lg-4">
                                   <label class="ckbox">
                                       <input type="checkbox" name="hot_deal" value="1">
                                       <span class="">Hot Deal</span>
                                   </label>
                               </div>
                               <div class="col-lg-4 ">
                                   <label class="ckbox">
                                       <input type="checkbox" value="1" name="best_rated">
                                       <span class="">Best rated</span>
                                   </label>
                               </div>
                               <div class="col-lg-4">
                                   <label class="ckbox">
                                       <input type="checkbox" value="1" name="mid_slider">
                                       <span class="">Mid Slider</span>
                                   </label>
                               </div>
                               <div class="col-lg-4 ">
                                   <label class="ckbox">
                                       <input type="checkbox" value="1" name="hot_new">
                                       <span class="">Hot New</span>
                                   </label>
                               </div>
                               <div class="col-lg-4">
                                   <label class="ckbox">
                                       <input type="checkbox" value="1" name="trend">
                                       <span class="">Trend</span>
                                   </label>
                               </div>
                           </div>
                            <!-- -------------end slider checkbox -------------- -->
                        </div>
                        <div class="form-layout-footer">
                            <button type="submit" class="btn btn-info mg-r-5">Submit Form</button>
                        </div><!-- form-layout-footer -->
                    </form>
                <!-- row -->
            </div><!-- form-layout -->
        </div><!-- card -->
 </div><!-- sl-pagebody -->
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
 </script>
 <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" crossorigin="anonymous"></script>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="category_id"]').on('change', function(){
                var category_id = $(this).val();
                if(category_id) {
                    $.ajax({
                        url: "{{  url('/get/subcategory/') }}/"+category_id,
                        type:"GET",
                        dataType:"json",
                        success:function(data) {
                            var d =$('select[name="sub_category_id"]').empty();
                            $.each(data, function(key, value){
                                $('select[name="sub_category_id"]').append('<option value="'+ value.id +'">' + value.subcategory_name + '</option>');
                            });
                        },
                    });
                } else {
                    alert('danger');
                }
            });
        });
    </script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#one')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script type="text/javascript">
        function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#two')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script type="text/javascript">
        function readURL2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#three')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        $(function(){
            'use strict';

            // Inline editor
            var editor = new MediumEditor('.editable');

            // Summernote editor
            $('#summernote').summernote({
                height: 150,
                tooltip: false
            })
        });
    </script>
@endsection
