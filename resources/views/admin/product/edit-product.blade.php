@extends('admin.master')
@section('title')
    Edit Product
@endsection
@section('content')
{{--    @php--}}
{{--    $brands = DB::table('brands')->get();--}}
{{--    $categories = DB::table('categories')->get();--}}
{{--    $subCategory = DB::table('subcategories')->get();--}}
{{--    @endphp--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" crossorigin="anonymous">
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Update Product</h5>
        </div><!-- sl-page-title -->
        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Update Product</h6>
            <p class="mg-b-20 mg-sm-b-30">Star mark Filed must required</p>

            <div class="form-layout">
                <form action="{{URL::to('update/product/without-photo',$product->id)}}" method="post">
                    @csrf
                    <div class="row mg-b-25">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Name: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="product_name" value="{{$product->product_name}}" placeholder="Product Name">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Code: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" value="{{$product->product_code}}" name="product_code" value="" placeholder="Product Code">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Quantity: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" value="{{$product->product_quantity}}" name="product_quantity"  placeholder="Product Quantity">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Category: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" name="category_id" data-placeholder="Choose Category">
                                    <option>Choose Category</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}"
                                            @php if ($product->category_id == $category->id) {
                                                echo "selected";
                                            }
                                            @endphp>{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Sub Category: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" name="sub_category_id" data-placeholder="Choose Subcategory">
                                    @foreach($subCategory as $subCategory)
                                        <option value="{{$subCategory->id}}" <?php if ($product->sub_category_id == $subCategory->id) {
                                            echo "selected";
                                        } ?>>{{$subCategory->subcategory_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Brand: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" name="brand_id" data-placeholder="Choose Brand">
                                    <option label="Select Brand Name"></option>
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}" <?php if($product->brand_id== $brand->id) {
                                            echo "selected";
                                        }?>>{{$brand->brand_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Product Size: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" id="size" value="{{$product->product_size}}" data-role="tagsinput" name="product_size" placeholder="Product Size">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Product Color: <span class="tx-danger">*</span></label>
                                <input class="form-control" data-role="tagsinput" id="color"type="text" id="size" data-role="tagsinput" value="{{$product->product_colors}}" name="product_colors" placeholder="Product Color">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Selling Price: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" id="size" value="{{$product->selling_price}}"  name="selling_price" placeholder="Product Selling Prize">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Discount Price: <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" id="size" value="{{$product->discount_price}}"  name="discount_price" placeholder="Product Selling Prize">
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Product Details: <span class="tx-danger">*</span></label>
                                <textarea class="form-control" id="summernote" name="product_details">
                                    {{$product->product_details}}
                                    </textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Video Link: <span class="tx-danger">*</span></label>
                                <input class="form-control" value="{{$product->video_link}}" name="video_link">
                            </div>
                        </div>
                        <!-- -------------col-4 image upload -------------- -->
                        <br>
                        <br>
                        <br>
                        <br>
                        <hr>
                        <!-- -------------end image upload -------------- -->
                        <!-- -------------slider checkbox -------------- -->
                        <div class="row mg-t-40 mg-lg-t-0 ml-2" style="margin-top: 40px">
                            <div class="col-lg-4">
                                <label class="ckbox">
                                    <input type="checkbox" <?php if ($product->main_slider==1){
                                        echo "checked";
                                    } ?> name="main_slider" value="1">
                                    <span class="">Main Slider</span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label class="ckbox">
                                    <input type="checkbox" <?php if ($product->hot_deal==1){
                                        echo "checked";
                                    } ?> name="hot_deal" value="1">
                                    <span class="">Hot Deal</span>
                                </label>
                            </div>
                            <div class="col-lg-4 ">
                                <label class="ckbox">
                                    <input type="checkbox" value="1"  <?php if ($product->best_rated==1){
                                        echo "checked";
                                    } ?>  name="best_rated">
                                    <span class="">Best rated</span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label class="ckbox">
                                    <input type="checkbox" value="1"  <?php if ($product->mid_slider==1){
                                        echo "checked";
                                    } ?>  name="mid_slider">
                                    <span class="">Mid Slider</span>
                                </label>
                            </div>
                            <div class="col-lg-4 ">
                                <label class="ckbox">
                                    <input type="checkbox" value="1"  <?php if ($product->hot_new==1){
                                        echo "checked";
                                    } ?>  name="hot_new">
                                    <span class="">Hot New</span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label class="ckbox">
                                    <input type="checkbox"  <?php if ($product->trend==1){
                                        echo "checked";
                                    } ?>  value="1" name="trend">
                                    <span class="">Trend</span>
                                </label>
                            </div>
                        </div>
                        <!-- -------------end slider checkbox -------------- -->
                    </div>
                    <div class="form-layout-footer">
                        <button type="submit" class="btn btn-info mg-r-5">Submit Form</button>
                    </div><!-- form-layout-footer -->
                </form>
                <!-- row -->
            </div><!-- form-layout -->
        </div><!-- card -->
    </div><!-- sl-pagebody -->
{{--for photo update--}}
<div class="sl-pagebody">
    <div class="card pd-20 pd-sm-40">
        <h6 class="card-body-title">Update Product With Photo</h6>
        <form action="{{ url('update/product/photo/'.$product->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <lebel>Image One (Main Thumbnail)<span class="tx-danger">*</span></lebel><br>
                        <label class="custom-file">
                            <input type="file" id="file" class="custom-file-input" name="image_one" onchange="readURL(this);"  accept="image">
                            <span class="custom-file-control"></span>
                            <input type="hidden" name="old_one" value="{{ $product->image_one }}"><br>
                            <img src="#" id="one" >
                        </label>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <img src="{{ URL::to($product->image_one) }}" style="height: 80px; width: 80px;">
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <lebel>Image Two <span class="tx-danger">*</span></lebel><br>
                    <label class="custom-file">
                        <input type="file" id="file" class="custom-file-input" name="image_two" onchange="readURL1(this);"  accept="image">
                        <input type="hidden" name="old_two" value="{{ $product->image_two }}"><br>
                        <span class="custom-file-control"></span>
                        <img src="#" id="two" >
                    </label>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <img src="{{ URL::to($product->image_two) }}" style="height: 80px; width: 80px;">
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <lebel>Image Three <span class="tx-danger">*</span></lebel><br>
                    <label class="custom-file">
                        <input type="file" id="file" class="custom-file-input" name="image_three" onchange="readURL2(this);"  accept="image">
                        <input type="hidden" name="old_three" value="{{ $product->image_three }}"><br>
                        <span class="custom-file-control"></span>
                        <img src="#" id="three" >
                    </label>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <img src="{{ URL::to($product->image_three) }}" style="height: 80px; width: 80px;">
                </div>
            </div>
            <br>
            <br>
                <button type="submit" class="btn btn-sm btn-warning">Update Photo</button>
        </form>

    </div>
</div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    </script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" crossorigin="anonymous"></script>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="category_id"]').on('change', function(){
                var category_id = $(this).val();
                if(category_id) {
                    $.ajax({
                        url: "{{  url('/get/subcategory/') }}/"+category_id,
                        type:"GET",
                        dataType:"json",
                        success:function(data) {
                            var d =$('select[name="sub_category_id"]').empty();
                            $.each(data, function(key, value){
                                $('select[name="sub_category_id"]').append('<option value="'+ value.id +'">' + value.subcategory_name + '</option>');
                            });
                        },
                    });
                } else {
                    alert('danger');
                }
            });
        });
    </script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#one')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script type="text/javascript">
        function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#two')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script type="text/javascript">
        function readURL2(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#three')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        $(function(){
            'use strict';

            // Inline editor
            var editor = new MediumEditor('.editable');

            // Summernote editor
            $('#summernote').summernote({
                height: 150,
                tooltip: false
            })
        });
    </script>
@endsection
