@extends('admin.master')
@section('title')
    View Product Details
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Product Details</h5>
        </div><!-- sl-page-title -->
        <div class="card pd-20 pd-sm-40">
            <div class="form-layout">
                    <div class="row mg-b-25">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Name: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->product_name}}</strong>
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Code: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->product_code}}</strong>
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="form-control-label">Product Quantity: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->product_quantity}}</strong>
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Category: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->category_name}}</strong>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Sub Category: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->subcategory_name}}</strong>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Brand: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->brand_name}}</strong>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Product Size: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->product_size}}</strong>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Product Color: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->product_colors}}</strong>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Selling Price: <span class="tx-danger">*</span></label>
                                <br>
                                <strong>{{$product->selling_price}}</strong>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mg-b-10-force" style="border: 2px solid gray">
                                <label class="form-control-label">Product Details: <span class="tx-danger">*</span></label>
                                <br>
                                <p>{!! $product->product_details !!}</p>
                            </div>
                        </div>
                        <!-- -------------col-4 image upload -------------- -->
                        <div class="col-lg-3">
                            <label class="form-control-label">Image One(Main thumbnail): <span class="tx-danger">*</span></label>
                            <label class="custom-file">
                                <img src="{{URL::to($product->image_one)}}" width="60px" height="80px" alt="">
                            </label>
                        </div><!-- col -->
                        <div class="col-lg-3 mg-t-40 mg-lg-t-0">
                            <label class="form-control-label">ImageTwo: <span class="tx-danger">*</span></label>
                            <br>
                            <label class="custom-file">
                                <img src="{{URL::to($product->image_two)}}" width="60px" height="80px" alt="">
                            </label>
                        </div><!-- col -->
                        <div class="col-lg-3 mg-t-40 mg-lg-t-0">
                            <label class="form-control-label">Image Three: <span class="tx-danger">*</span></label>
                            <br>
                            <img src="{{URL::to($product->image_three)}}" width="60px" height="80px" alt="">
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <hr>
                        <!-- -------------end image upload -------------- -->
                        <!-- -------------slider checkbox -------------- -->
                        <div class="row mg-t-40 mg-lg-t-0 ml-2" style="margin-top: 40px">
                            <div class="col-lg-4">
                                <label class="">
                                    @if($product->main_slider==1)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                    <span class="">Main Slider</span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    @if($product->hot_deal==1)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                    <span class="">Hot Deal</span>
                                </label>
                            </div>
                            <div class="col-lg-4 ">
                                <label class="">
                                    @if($product->hot_deal==1)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                    <span class="">Best rated</span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    @if($product->mid_slider==1)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                    <span class="">Mid Slider</span>
                                </label>
                            </div>
                            <div class="col-lg-4 ">
                                <label class="">
                                    @if($product->hot_new==1)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                    <span class="">Hot New</span>
                                </label>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    @if($product->trend==1)
                                        <span class="badge badge-success">Active</span>
                                    @else
                                        <span class="badge badge-danger">Inactive</span>
                                    @endif
                                    <span class="">Trend</span>
                                </label>
                            </div>
                        </div>
                        <!-- -------------end slider checkbox -------------- -->
                    </div>
                <!-- row -->
        </div><!-- card -->
    </div><!-- sl-pagebody -->
    </div><!-- sl-pagebody -->
@endsection


