@extends('admin.master')
@section('title')
    Coupon
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Coupon Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Coupon List</h6>
            <br>
            <form action="{{route('update-coupon')}}" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Coupon</h5>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category-form">Coupon Code</label>
                            <input type="text" class="form-control" value="{{$coupon->coupon_code}}" name="coupon_code" placeholder="coupon code">
                            <input type="hidden" class="form-control" value="{{$coupon->id}}" name="id" placeholder="coupon code">
                        </div>
                        <div class="form-group">
                            <label for="category-form">Coupon discount</label>
                            <input type="text" class="form-control" value="{{$coupon->coupon_discount}}" name="coupon_discount" placeholder="coupon discount">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div><!-- card -->
    </div>
@endsection
