@extends('admin.master')
@section('title')
    Coupon
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Coupon Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Coupon List
                <a href="" class="btn btn-sm btn-warning" style="float: right;" data-toggle="modal"
                   data-target="#exampleModal">Add New</a>
            </h6>
            <br>
            <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-15p">ID</th>
                        <th class="wd-15p">Coupon Code</th>
                        <th class="wd-15p">Coupon Percentage</th>
                        <th class="wd-20p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($coupons as $coupon)
                            <td>{{$coupon->id}}</td>
                            <td>{{$coupon->coupon_code}}</td>
                            <td>{{$coupon->coupon_discount}}</td>
                            <td>
                                <a href="{{URL::to('edit/coupon',$coupon->id)}}" class="btn btn-sm btn-info">Edit</a>
                                <a href="{{URL::to('delete/coupon',$coupon->id)}}" class="btn btn-sm btn-danger" onclick="sweetalartClick()" id="delete">Delete</a>
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div><!-- card -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{route('store.coupon')}}" method="POST">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Coupon</h5>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="category-form">Coupon Code</label>
                                <input type="text" name="coupon_code" placeholder="coupon Code">
                            </div>
                            <div class="form-group">
                                <label for="category-form">Coupon Code</label>
                                <input type="text" name="coupon_discount" placeholder="Discount %">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endsection
        @section('script')
            <script>
                $(function(){
                    'use strict';
                    $('#datatable1').DataTable({
                        responsive: true,
                        language: {
                            searchPlaceholder: 'Search...',
                            sSearch: '',
                            lengthMenu: '_MENU_ items/page',
                        }
                    });
                });
            </script>
@endsection

