@extends('admin.master')
@section('title')
    Post Category
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Post Category Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Post Category List
                <a href="" class="btn btn-sm btn-warning" style="float: right;" data-toggle="modal"
                   data-target="#exampleModal">Add New</a>
            </h6>
            <br>
            <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-15p">ID</th>
                        <th class="wd-15p">Post Category(English)</th>
                        <th class="wd-15p">Post Category(English)</th>
                        <th class="wd-20p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($postCategories as $postCategory)
                            <td>{{$postCategory->id}}</td>
                            <td>{{$postCategory->category_name_en}}</td>
                            <td>{{$postCategory->category_name_bn}}</td>
                            <td>
                                <a href="{{URL::to('edit/post/category',$postCategory->id)}}" class="btn btn-sm btn-info">Edit</a>
                                <a href="{{URL::to('delete/post/category',$postCategory->id)}}" class="btn btn-sm btn-danger" id="delete">Delete</a>
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div><!-- card -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{route('store.post.category')}}" method="POST">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add New Post Category</h5>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="category-form">Post Category Name(English)</label>
                                <input type="text" name="category_name_en" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="category-form">Post Category Name(Bangla)</label>
                                <input type="text" name="category_name_bn" placeholder="">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endsection
        @section('script')
            <script>
                $(function(){
                    'use strict';
                    $('#datatable1').DataTable({
                        responsive: true,
                        language: {
                            searchPlaceholder: 'Search...',
                            sSearch: '',
                            lengthMenu: '_MENU_ items/page',
                        }
                    });
                });
            </script>
@endsection


