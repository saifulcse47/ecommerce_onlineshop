@extends('admin.master')
@section('title')
    Create Post Category
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Add New Post Category</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h4 class="card-body-title" style="text-align: center">Add new Post Category</h4>
            <br>
            <form action="{{route('store.post.category')}}" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Post Category</h5>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category-form"><strong class="text-dark">Category Name(English)</strong></label>
                            <input type="text" class="form-control" name="category_name_en" placeholder="category name (english)">
                        </div>
                        <div class="form-group">
                            <label for="category-form"><strong class="text-dark">Category Name(Bangla)</strong></label>
                            <input type="text" class="form-control" name="category_name_bn" placeholder="category name (bangla)">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div><!-- card -->
    </div>
@endsection
