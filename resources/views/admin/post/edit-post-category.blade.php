@extends('admin.master')
@section('title')
    Edit Post Category
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Edit Post Category</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h4 class="card-body-title" style="text-align: center">Edit Post Category</h4>
            <br>
            <form action="{{route('update.post.category')}}" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Post Category</h5>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category-form"><strong class="text-dark">Category Name(English)</strong></label>
                            <input type="text" class="form-control" value="{{$postCategory->category_name_en}}" name="category_name_en" placeholder="category name (english)">
                            <input type="hidden" class="form-control" value="{{$postCategory->id}}" name="id" placeholder="category name (english)">
                        </div>
                        <div class="form-group">
                            <label for="category-form"><strong class="text-dark">Category Name(Bangla)</strong></label>
                            <input type="text" class="form-control" value="{{$postCategory->category_name_bn}}" name="category_name_bn" placeholder="category name (bangla)">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </div>
            </form>
        </div><!-- card -->
    </div>
@endsection
