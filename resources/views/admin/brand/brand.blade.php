@extends('admin.master')
@section('title')
    Brand
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Brand Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Brand List
                <a href="" class="btn btn-sm btn-warning" style="float: right;" data-toggle="modal"
                   data-target="#exampleModal">Add New</a>
            </h6>
            <br>
            <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-15p">ID</th>
                        <th class="wd-15p">Band name</th>
                        <th class="wd-15p">Band Logo</th>
                        <th class="wd-20p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($brands as $brand)
                            <td>{{$brand->id}}</td>
                            <td>{{$brand->brand_name}}</td>
                            <td><img src="{{URL::to($brand->brand_logo)}}" height="80" width="70" alt=""></td>
                            <td>
                                <a href="{{URL::to('edit/brand',$brand->id)}}" class="btn btn-sm btn-info">Edit</a>
                                <a href="{{URL::to('delete/brand',$brand->id)}}" class="btn btn-sm btn-danger" onclick="sweetalartClick()" id="delete">Delete</a>
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div><!-- card -->
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="{{route('store.brand')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Band</h5>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="band-form">Brand Name</label>
                                <input type="text" name="brand_name" placeholder="Band name">
                            </div>
                            <div class="form-group">
                                <label for="band-form">Brand Logo</label>
                                <input type="file" name="brand_logo" placeholder="Band logo">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @endsection
        @section('script')
            <script>
                $(function(){
                    'use strict';
                    $('#datatable1').DataTable({
                        responsive: true,
                        language: {
                            searchPlaceholder: 'Search...',
                            sSearch: '',
                            lengthMenu: '_MENU_ items/page',
                        }
                    });
                });
            </script>
@endsection

