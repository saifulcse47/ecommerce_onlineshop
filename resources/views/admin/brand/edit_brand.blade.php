@extends('admin.master')
@section('title')
    Edit Brand
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Brand Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Brand List</h6>
            <br>
            <form action="{{URL::To('update/brand',$brand->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Brand</h5>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category-form">Brand Name</label>
                            <input type="text" class="form-control" value="{{$brand->brand_name}}" name="brand_name" placeholder="Brand name">
                        </div>
                        <div class="form-group">
                            <label for="category-form">Brand Logo</label>
                            <input type="file" class="form-control" value="" name="brand_logo" placeholder="Brand Logo">
                            <br>
                            <img src="{{asset($brand->brand_logo)}}" height="60" width="50" alt="">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div><!-- card -->
@endsection

