@extends('admin.master')
@section('title')
    SubCategory
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Category Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">SubCategory List</h6>
            <br>
            <form action="{{URL::To('update/category',$subcategory->id)}}" method="POST">
                @csrf
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit SubCategory</h5>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category-form">SubCategory Name</label>
                            <input type="text" class="form-control" value="{{$subcategory->subcategory_name}}" name="category_name" placeholder="category name">
                        </div>
                        <div class="form-group">
                            <label for="category-form">Category Name</label>
                            <select class="form-control" name="category_id" id="">
                                @foreach($categories as $category)
                                <option  value="{{$category->id}}"
                                         @php
                                             if ($category->id == $subcategory->id){
                                                 echo "selected";
                                             }
                                          @endphp
                                >
                                    {{$category->category_name}}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer" style="background-color: #7f7f7f">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
            </form>
        </div><!-- card -->
@endsection
