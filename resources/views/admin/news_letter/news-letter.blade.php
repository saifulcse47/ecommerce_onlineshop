@extends('admin.master')
@section('title')
    Category
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>News Letter Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">NewsLetters List
                <a href="" class="btn btn-sm btn-danger" id="delete" style="float: right;">ALL Delete</a>
            </h6>
            <br>
            <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-15p">ID</th>
                        <th class="wd-15p">Email</th>
                        <th class="wd-15p">Subscribe time </th>
                        <th class="wd-20p">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($newsLetters as $newsLetter)
                            <td><input type="checkbox">{{$newsLetter->id}}</td>
                            <td>{{$newsLetter->email}}</td>
                            <td>{{\Carbon\Carbon::parse($newsLetter->created_at)->diffForHumans()}}</td>
                            <td>
                                <a href="{{URL::to('delete/newsletter',$newsLetter->id)}}" class="btn btn-sm btn-danger"  id="delete">Delete</a>
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div><!-- card -->
        </div><!-- card -->
        <!-- Modal -->
@endsection
@section('script')
    <script>
        $(function(){
            'use strict';
            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });
    </script>
@endsection
