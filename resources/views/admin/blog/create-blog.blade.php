@extends('admin.master')
@section('title')
    Add Blog Post
@endsection
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" crossorigin="anonymous">
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>ADD Blog Post</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Add New Blog Post</h6>
            <p class="mg-b-20 mg-sm-b-30">Star mark Filed must required</p>
            <div class="form-layout">
                <form action="{{route('store.blog.post')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row mg-b-25">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">Post Title(English): <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="post_title_en" value="" placeholder="Product Name">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="form-control-label">Post Title(Bangla): <span class="tx-danger">*</span></label>
                                <input class="form-control" type="text" name="post_title_bn" value="" placeholder="Product Code">
                            </div>
                        </div><!-- col-4 -->
                        <div class="col-lg-6">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Category: <span class="tx-danger">*</span></label>
                                <select class="form-control select2" name="category_id" data-placeholder="Choose Category">
                                    <option>Choose Category</option>
                                    @foreach($postCategories as $postCategory)
                                        <option value="{{$postCategory->id}}">{{$postCategory->category_name_en}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Product Details(English): <span class="tx-danger">*</span></label>
                                <textarea class="form-control" id="summernote" name="post_details_en">

                                    </textarea>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group mg-b-10-force">
                                <label class="form-control-label">Product Details(Bangla): <span class="tx-danger">*</span></label>
                                <textarea class="form-control" id="summernoteOne" name="post_details_bn">

                                    </textarea>
                            </div>
                        </div>
                        <!-- -------------col-4 image upload -------------- -->
                        <div class="col-lg-12">
                            <label class="form-control-label">Image One(Main thumbnail): <span class="tx-danger">*</span></label>
                            <br>
                            <label class="custom-file">
                                <input type="file" id="file" name="post_image" class="custom-file-input" onchange="readURL(this);" required="" accept="image">
                                <span class="custom-file-control"></span>
                                <br>
                                <img src="#" id="one" alt="">
                            </label>
                        </div><!-- col -->
                        <br>
                        <br>
                        <br>
                        <br>
                        <hr>
                        <!-- -------------end image upload -------------- -->
                    </div>
                    <br>
                    <div class="form-layout-footer">
                        <button type="submit" class="btn btn-info mg-r-5">Submit Form</button>
                    </div><!-- form-layout-footer -->
                </form>
                <!-- row -->
            </div><!-- form-layout -->
        </div><!-- card -->
    </div><!-- sl-pagebody -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    </script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" crossorigin="anonymous"></script>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $('select[name="category_id"]').on('change', function(){
                var category_id = $(this).val();
                if(category_id) {
                    $.ajax({
                        url: "{{  url('/get/subcategory/') }}/"+category_id,
                        type:"GET",
                        dataType:"json",
                        success:function(data) {
                            var d =$('select[name="sub_category_id"]').empty();
                            $.each(data, function(key, value){
                                $('select[name="sub_category_id"]').append('<option value="'+ value.id +'">' + value.subcategory_name + '</option>');
                            });
                        },
                    });
                } else {
                    alert('danger');
                }
            });
        });
    </script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#one')
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <script>
        $(function(){
            'use strict';

            // Inline editor
            var editor = new MediumEditor('.editable');

            // Summernote editor
            $('#summernote').summernote({
                height: 150,
                tooltip: false
            })
        });
    </script>
    <script>
        $(function(){
            'use strict';

            // Inline editor
            var editor = new MediumEditor('.editable');

            // Summernote editor
            $('#summernoteOne').summernote({
                height: 150,
                tooltip: false
            })
        });
    </script>
@endsection

