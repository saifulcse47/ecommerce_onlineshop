
@extends('admin.master')
@section('title')
    All Blog Post
@endsection
@section('content')
    <div class="sl-pagebody">
        <div class="sl-page-title">
            <h5>Blog Post Table</h5>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
            <h6 class="card-body-title">Product List
                <a href="{{route('admin.add-product')}}" class="btn btn-sm btn-warning" style="float: right;"
                >Add New</a>
            </h6>
            <br>
            <div class="table-wrapper">
                <table id="datatable1" class="table display responsive nowrap">
                    <thead>
                    <tr>
                        <th class="wd-15p text-center">Post Title(EN)</th>
                        <th class="wd-15p text-center">Image</th>
                        <th class="wd-15p text-center">Post Category</th>
                        <th class="wd-20p text-center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($blogPosts as $blogPost)
                            <td class="text-center">{{$blogPost->post_title_en}}</td>
                            <td class="text-center">
                                <img src="{{URL::to($blogPost->post_image)}}" height="60px" width="50px" alt="">
                            </td>
                            <td class="text-center">{{$blogPost->category_name_en}}</td>
                            <td class="text-center">
                                <a href="{{URL::to('edit/post/blog',$blogPost->id)}}" class="btn btn-info" title="Edit"> <i class="fas fa-edit"></i></a>
                                <a href="{{URL::to('delete/post',$blogPost->id)}}" class="btn btn-danger" title="Delete" id="delete"><i class="fas fa-trash-alt"></i></a>
                                <a href="{{URL::to('view/product',$blogPost->id)}}" class="btn btn-warning" title="view"><i class="fas fa-eye"></i></a>
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div><!-- table-wrapper -->
        </div><!-- card -->
    </div><!-- card -->
@endsection
@section('script')
    <script>
        $(function(){
            'use strict';
            $('#datatable1').DataTable({
                responsive: true,
                language: {
                    searchPlaceholder: 'Search...',
                    sSearch: '',
                    lengthMenu: '_MENU_ items/page',
                }
            });
        });
    </script>
@endsection
