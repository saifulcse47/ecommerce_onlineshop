@php
$sliders=DB::table('products')
->join('brands','products.brand_id','brands.id')
->select('products.*','brands.brand_name')
->where('products.main_slider',1)
->orderBy('id','DESC')->first()
    @endphp
<div class="banner">
    <div class="banner_background" style="background-image:url({{asset('/')}}frontend/images/banner_background.jpg)"></div>
    <div class="container fill_height">
        <div class="row fill_height">
            <div class="banner_product_image"><img src="{{asset($sliders->image_one)}}" alt=""></div>
            <div class="col-lg-5 offset-lg-4 fill_height">
                <div class="banner_content">
                    <h1 class="banner_text">new era of smartphones</h1>
                    <div class="banner_price"><span>$530</span>${{$sliders->selling_price}}</div>
                    <div class="banner_product_name">{{$sliders->product_name}}</div>
                    <div class="button banner_button"><a href="#">Shop Now</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
