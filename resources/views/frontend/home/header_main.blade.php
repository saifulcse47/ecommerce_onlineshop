 <div class="row">
                <div class="header-top col-md-12 col-sm-12 col-lg-12">
                    <div class="item">
                        <ul>
                            <li><a href="">+00000000<i class="fas fa-phone-volume"></i></a></li>
                            <li><a href="">Wishlist<i class="fas fa-heart"></i></a></li>
                            <li><a href="">Track your order<i class="fas fa-truck-moving"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="middle-header">
                           <div class="row col-md-12">
                    <div class="col-md-3 menu-logo">
                        <a class="navbar-brand" href="#"><img src="{{asset('assets/frontend/image/logo/rsz_logoengname.png')}}"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
                        </button>
                    </div>
                    <div class="col-md-9 float-left collapse navbar-collapse" id="navbarSupportedContent">
                        <div class="row">
                            <div class="col-md-7 row search-box">
                                <input class="form-control mr-sm-2" type="search" placeholder="What are you looking for?" aria-label="Search">
                                <button class="btn my-2 my-sm-0" type="submit"><i class="fas fa-search-plus"></i></button>
                            </div>
                            <div class="col-md-5 mid-menu-item">
                                <ul>
                                    <li><a href="">Item<i class="fas fa-cart-plus"></i></a></li>
                                    <li><a href="">Login<i class="fas fa-user"></i></a></li>
                                    <li><a href="">Help<i class="fas fa-question-circle"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>