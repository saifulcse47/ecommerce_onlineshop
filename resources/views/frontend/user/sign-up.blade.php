<form action="">

    <input type="text" name="" id="nm1" placeholder="Enter your name">
    <p class="err_msg text-danger" id="nm_err"></p>
    <input type="email" name="" id="em1" placeholder="Enter your Email">
    <p class="err_msg" id="em_err"></p>
    <input type="password" id="ps1" placeholder="Enter your pass">
    <p class="err_msg" id="ps1_err"></p>
    <input type="password" id="cps1" placeholder="Confirm your pass">
    <p class="err_msg" id="cps1_err"></p>
    <input type="text" placeholder="Write something" required>

    <div class="btn">
        <button onclick="return valid();">submit</button>

    </div>

</form>
{{--<!DOCTYPE html>--}}
{{--<html>--}}
{{--<head>--}}
{{--    <title>Educational registration form</title>--}}
{{--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">--}}
{{--    <style>--}}
{{--        html, body {--}}
{{--            min-height: 100%;--}}
{{--        }--}}
{{--        body, div, form, input, select, p {--}}
{{--            padding: 0;--}}
{{--            margin: 0;--}}
{{--            outline: none;--}}
{{--            font-family: Roboto, Arial, sans-serif;--}}
{{--            font-size: 16px;--}}
{{--            color: #eee;--}}
{{--        }--}}
{{--        body {--}}
{{--            background: url("#") no-repeat center;--}}
{{--            background-size: cover;--}}
{{--        }--}}
{{--        h1, h2 {--}}
{{--            text-transform: uppercase;--}}
{{--            font-weight: 400;--}}
{{--        }--}}
{{--        h2 {--}}
{{--            margin: 0 0 0 8px;--}}
{{--        }--}}
{{--        .main-block {--}}
{{--            display: flex;--}}
{{--            flex-direction: column;--}}
{{--            justify-content: center;--}}
{{--            align-items: center;--}}
{{--            height: 100%;--}}
{{--            padding: 25px;--}}
{{--            background: rgba(0, 0, 0, 0.5);--}}
{{--        }--}}
{{--        .left-part, form {--}}
{{--            padding: 25px;--}}
{{--        }--}}
{{--        .left-part {--}}
{{--            text-align: center;--}}
{{--        }--}}
{{--        .fa-graduation-cap {--}}
{{--            font-size: 72px;--}}
{{--        }--}}
{{--        form {--}}
{{--            background: rgba(0, 0, 0, 0.7);--}}
{{--        }--}}
{{--        .title {--}}
{{--            display: flex;--}}
{{--            align-items: center;--}}
{{--            margin-bottom: 20px;--}}
{{--        }--}}
{{--        .info {--}}
{{--            display: flex;--}}
{{--            flex-direction: column;--}}
{{--        }--}}
{{--        input, select {--}}
{{--            padding: 5px;--}}
{{--            margin-bottom: 30px;--}}
{{--            background: transparent;--}}
{{--            border: none;--}}
{{--            border-bottom: 1px solid #eee;--}}
{{--        }--}}
{{--        input::placeholder {--}}
{{--            color: #eee;--}}
{{--        }--}}
{{--        option:focus {--}}
{{--            border: none;--}}
{{--        }--}}
{{--        option {--}}
{{--            background: black;--}}
{{--            border: none;--}}
{{--        }--}}
{{--        .checkbox input {--}}
{{--            margin: 0 10px 0 0;--}}
{{--            vertical-align: middle;--}}
{{--        }--}}
{{--        .checkbox a {--}}
{{--            color: #26a9e0;--}}
{{--        }--}}
{{--        .checkbox a:hover {--}}
{{--            color: #85d6de;--}}
{{--        }--}}
{{--        .btn-item, button {--}}
{{--            padding: 10px 5px;--}}
{{--            margin-top: 20px;--}}
{{--            border-radius: 5px;--}}
{{--            border: none;--}}
{{--            background: #26a9e0;--}}
{{--            text-decoration: none;--}}
{{--            font-size: 15px;--}}
{{--            font-weight: 400;--}}
{{--            color: #fff;--}}
{{--        }--}}
{{--        .btn-item {--}}
{{--            display: inline-block;--}}
{{--            margin: 20px 5px 0;--}}
{{--        }--}}
{{--        button {--}}
{{--            width: 100%;--}}
{{--        }--}}
{{--        button:hover, .btn-item:hover {--}}
{{--            background: #85d6de;--}}
{{--        }--}}
{{--        @media (min-width: 568px) {--}}
{{--            html, body {--}}
{{--                height: 100%;--}}
{{--            }--}}
{{--            .main-block {--}}
{{--                flex-direction: row;--}}
{{--                height: calc(100% - 50px);--}}
{{--            }--}}
{{--            .left-part, form {--}}
{{--                flex: 1;--}}
{{--                height: auto;--}}
{{--            }--}}
{{--        }--}}
{{--    </style>--}}
{{--</head>--}}
{{--<body>--}}
{{--<div class="main-block">--}}
{{--    <div class="left-part">--}}
{{--        <i class="fas fa-graduation-cap"></i>--}}
{{--        <h1>welcome to Register</h1>--}}
{{--        <p>Already you have a acount ?If you have account sign in</p>--}}
{{--        <div class="btn-group">--}}
{{--            <a class="btn-item" href="#">Sign In</a>--}}
{{--            <a class="btn-item" href="#">Select Quiz</a>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <form method="POST" action="{{route('store.user')}}">--}}
{{--        @csrf--}}
{{--        <div class="title">--}}
{{--            <i class="fas fa-pencil-alt"></i>--}}
{{--            <h2>Register here</h2>--}}
{{--        </div>--}}
{{--        <div class="info">--}}
{{--            <input  class="fname" type="text" name="user_name" id="user_name" placeholder="Full name">--}}
{{--            <p class="text-danger" style="color: red;font-style: italic" id="name_error"></p>--}}
{{--            <input type="text" name="user_email" id="user_email" placeholder="Email">--}}
{{--            <p class="text-danger" style="color: red;font-style: italic" id="email_error"></p>--}}
{{--            <input type="text" name="user_phone" id="user_phone" placeholder="Phone number">--}}
{{--            <p class="text-danger" style="color: red;font-style: italic" id="phone_error"></p>--}}
{{--            <input type="password" name="user_password" id="user_password" placeholder="Password">--}}
{{--            <p class="text-danger" style="color: red;font-style: italic" id="password_error"></p>--}}
{{--            <input type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password">--}}
{{--            <p class="text-danger" style="color: red;font-style: italic" id="confirm_password_error"></p>--}}
{{--            <div class="form-group">--}}
{{--                <label class="control-label col-md-3">Gender</label>--}}
{{--                <br><br>--}}
{{--                <div class="col-md-9 radio">--}}
{{--                    <label><input type="radio" id="gender" name="gender" value="1"/>Male</label>--}}
{{--                    <label><input type="radio" id="gender" name="gender" value="0"/>Female</label>--}}
{{--                    <p class="text-danger" style="color: red;font-style: italic" id="gender_error"></p>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="checkbox">--}}
{{--            <input type="checkbox" id="check_box" name="checkbox"><span>I agree to the <a href="">Privacy Poalicy for this site.</a></span>--}}
{{--            <p class="text-danger" style="color: red;font-style: italic" id="check_box_error"></p>--}}
{{--        </div>--}}
{{--        <input type="submit" class="btn" id="reg_btn" name="btn" value="Submit">--}}
{{--        <button onclick="return userFormValidation();" type="submit" id="regBtn" name="btn" >Submit</button>--}}
{{--    </form>--}}
{{--</div>--}}
{{--<script src="{{asset('/')}}frontend/js/main.js"></script>--}}
{{--</body>--}}
{{--</html>--}}
