<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Frontend\FrontendController@index');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['as'=>'admin.','prefix'=>'admin','namespace'=>'Admin','middleware'=>['auth','admin']],
    function () {
    Route::get('admin','DashboardController@index')->name('dashboard');
    });
Route::group(['as'=>'author.','prefix'=>'author','namespace'=>'Author','middleware'=>['auth','author']],
    function () {
    Route::get('dashboard','DashboardController@index')->name('dashboard');
    });

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('admin-dashboard',[
//    'uses' => 'AdminController@home',
//    'as' => 'admin-dashboard'
//]);
//admin panel route
//category
Route::get('admin/categories','Admin\category\CategoryController@category')->name('categories');
Route::post('admin/store/category','Admin\category\CategoryController@storeCategory')->name('store.category');
Route::get('delete/category/{id}','Admin\category\CategoryController@deleteCategory');
Route::get('edit/category/{id}','Admin\category\CategoryController@editCategory');
Route::post('update/category/{id}','Admin\category\CategoryController@updateCategory');
//bands
Route::get('admin/brandes','Admin\brand\BrandController@brand')->name('brands');
Route::post('admin/store/brandes','Admin\brand\BrandController@storeBrand')->name('store.brand');
Route::get('delete/brand/{id}','Admin\brand\BrandController@deleteBrand');
Route::get('edit/brand/{id}','Admin\brand\BrandController@editBrand');
Route::post('update/brand/{id}','Admin\brand\BrandController@updateBrand');
//subcategory
Route::get('admin/category/subcategory','Admin\subcategory\SubcategoryController@subCategory')->name('subcategories');
Route::post('admin/store/subcategory','Admin\subcategory\SubcategoryController@storeSubCategory')->name('store.subcategory');
Route::get('edit/sub-category/{id}','Admin\subcategory\SubcategoryController@editSubCategory');
Route::get('update/sub-category/{id}','Admin\subcategory\SubcategoryController@updateSubCategory');
Route::get('delete/sub-category/{id}','Admin\subcategory\SubcategoryController@deleteSubCategory');
//coupon ---------------------
Route::get('admin/coupon','Admin\CouponController@coupon')->name('admin.coupon');
Route::post('admin/store/coupon','Admin\CouponController@storeCoupon')->name('store.coupon');
Route::get('edit/coupon/{id}','Admin\CouponController@editCoupon');
Route::post('update/coupon','Admin\CouponController@saveUpdateCoupon')->name('update-coupon');
Route::get('delete/coupon/{id}','Admin\CouponController@deleteCoupon');
//admin newsletters
Route::get('admin/newsletter','Admin\CouponController@newsLetter')->name('admin.newsletter');
Route::get('delete/newsletter/{id}','Admin\CouponController@deleteNewsLetter');
//products========================================
Route::get('admin/product/add','Admin\product\ProductController@addProduct')->name('admin.add-product');
Route::get('admin/all/product','Admin\product\ProductController@allProduct')->name('admin.all-product');
Route::get('edit/product/{id}','Admin\product\ProductController@editProduct');
Route::post('update/product/without-photo/{id}','Admin\product\ProductController@updateProductWithoutPhoto');
Route::post('update/product/photo/{id}','Admin\product\ProductController@updateProductPhoto');
Route::get('delete/product/{id}','Admin\product\ProductController@deleteProduct');
Route::get('view/product/{id}','Admin\product\ProductController@viewProduct');
Route::get('inactive/product/{id}','Admin\product\ProductController@inactiveProduct');
Route::get('active/product/{id}','Admin\product\ProductController@activeProduct');
Route::post('admin/store/product','Admin\product\ProductController@storeProduct')->name('store.product');
// ===========get subcategory by ajax
Route::get('get/subcategory/{category_id}','Admin\product\ProductController@getSubCategory');

    //-------------post=============
Route::get('create/post/category','Admin\post\PostController@createPostCategory')->name('create.post.category');
Route::post('store/post/category','Admin\post\PostController@storePostCategory')->name('store.post.category');
Route::get('all/post/category','Admin\post\PostController@allPostCategory')->name('admin.all.post.category');
Route::get('edit/post/category/{id}','Admin\post\PostController@editPostCategory');
Route::post('update/post/category','Admin\post\PostController@updatePostCategory')->name('update.post.category');
Route::get('delete/post/category/{id}','Admin\post\PostController@deletePostCategory');
 //---------blog post--------
Route::get('create/blog/post','Admin\post\PostController@createPostBlog')->name('create.post');
Route::post('store/blog/post','Admin\post\PostController@storePostBlog')->name('store.blog.post');
Route::get('admin/all/blog/post','Admin\post\PostController@managePostBlog')->name('all.post.blog');
Route::get('delete/post/{id}','Admin\post\PostController@deletePostBlog');
Route::get('edit/post/blog/{id}','Admin\post\PostController@editPostBlog');
Route::post('update/post/blog/','Admin\post\PostController@updatePostBlog')->name('update.blog.post');











//Frontend ...................

//newsletter........
Route::post('store/newsletter','Frontend\FrontendController@newsLetter')->name('store.newsletter');
Route::get('user/sign-up','Frontend\SignUpController@index')->name('user.sign-up');
Route::get('user/sign-in','Frontend\SignUpController@SignIn')->name('sign-in');
Route::post('store/user','Frontend\SignUpController@storeUser')->name('store.user');








