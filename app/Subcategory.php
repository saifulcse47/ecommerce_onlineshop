<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $fillable = ['subcategory_name','category_id'];

    public static function storeSubCategory($request) {
        $validatedData = $request->validate([
            'subcategory_name' => 'required|max:55',
            'category_id' => 'required',
        ]);
        $subcate = new Subcategory();
        $subcate->subcategory_name      =$request->subcategory_name;
        $subcate->category_id           =$request->category_id;
        $subcate->save();
    }
    public static function updateSubCategory ($request,$notification) {
        $subcate = new Subcategory();
        $subcate->subcategory_name      =$request->subcategory_name;
        $subcate->category_id           =$request->category_id;
        $subcate->save();
        if ($subcate){
            $notification = array(
                'messege' => 'SubCategory Update Successfully.',
                'alert-type' => 'success'
            );
        }
        else{
            $notification = array(
                'messege' => 'Nothing to update.',
                'alert-type' => 'success'
            );
        }
    }
}
