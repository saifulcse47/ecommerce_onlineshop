<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsLetter extends Model
{
    protected $fillable =['email'];

    public static function newsLetterEmail ($request) {
        $validatedData = $request->validate([
            'email' => 'required|unique:news_letters|max:55',
        ]);
        $newsLetter             = new NewsLetter();
        $newsLetter->email      =$request->email;
        $newsLetter->save();
    }
}
