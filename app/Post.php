<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Post extends Model
{
    protected $fillable =['category_id','post_title_en','post_title_bn','post_image','post_details_en','post_details_bn'];

    public static function saveBlogPost($request) {
        $postImage      = $request->file('post_image');
        $postImageName=hexdec(uniqid()).'.'.$postImage->getClientOriginalExtension();
        Image::make($postImage)->resize(400,300)->save('media/post/'.$postImageName);
//        $directory = 'media/post/';
//        $postImage->move($directory,$postImageName);
        $blogPost = new Post();
        $blogPost->category_id          =$request->category_id;
        $blogPost->post_title_en        =$request->post_title_en;
        $blogPost->post_title_bn        =$request->post_title_bn;
        $blogPost->post_image           ='media/post/'.$postImageName;
        $blogPost->post_details_en      =$request->post_details_en;
        $blogPost->post_details_bn      =$request->post_details_bn;
        $blogPost->save();
    }
    public static function updateBlogPost($request) {
        $blogPost=Post::find($request->id);
        $postImage =$request->file('post_image');
        $OldImage    =$request->old_image;
        if ($postImage) {
            unlink($OldImage);
            $postImageName=hexdec(uniqid()).'.'.$postImage->getClientOriginalExtension();
            Image::make($postImage)->resize(400,300)->save('media/post/'.$postImageName);
            $blogPost->category_id          =$request->category_id;
            $blogPost->post_title_en        =$request->post_title_en;
            $blogPost->post_title_bn        =$request->post_title_bn;
            $blogPost->post_image           ='media/post/'.$postImageName;
            $blogPost->post_details_en      =$request->post_details_en;
            $blogPost->post_details_bn      =$request->post_details_bn;
        }
        else{
            $blogPost->category_id          =$request->category_id;
            $blogPost->post_title_en        =$request->post_title_en;
            $blogPost->post_title_bn        =$request->post_title_bn;
            $blogPost->post_details_en      =$request->post_details_en;
            $blogPost->post_details_bn      =$request->post_details_bn;
        }
        $blogPost->save();
    }
}
