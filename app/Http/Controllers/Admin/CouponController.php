<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use App\Http\Controllers\Controller;
use App\NewsLetter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class CouponController extends Controller
{
    public function coupon() {
        return view('admin.coupon.coupon',[
            'coupons' =>Coupon::all()
        ]);
    }
    public function storeCoupon(Request $request) {
        $coupon=Coupon::storeCoupon($request);
        $notification = array(
            'messege' => 'Coupon Insert Successfully.',
            'alert-type' => 'success'
        );
        return redirect()->route('admin.coupon')->with($notification);
    }
    public function editCoupon($id) {
        return view('admin.coupon.edit-coupon',[
            'coupon'=>Coupon::where('id',$id)->first()
        ]);
    }
    public function saveUpdateCoupon(Request $request) {

      $coupon= Coupon::updateCoupon($request);
        if ($coupon){
            $notification = array(
                'messege' =>'Update coupon done',
                'alert-type' =>'success',
            );
        }
        else{
            $notification = array(
                'messege' =>'Nothing to Update',
                'alert-type' =>'success',
            );
        }
        return redirect()->route('admin.coupon')->with($notification);
    }
    public function deleteCoupon($id) {
//            DB::table('subcategories')->where('id',$id)->delete();
//            $notification = array(
//                'messege' => 'Delete Subcategory done',
//                'alert-type' => 'success'
//            );
//            return redirect()->back()->with($notification);
//        echo "$id";
        DB::table('coupons')->where('id',$id)->delete();
        $notification = array(
            'messege' =>'delete coupon done',
            'alert-type' =>'success',
        );
        return redirect()->back()->with($notification);
    }
    public function newsLetter() {
        return view('admin.news_letter.news-letter',[
            'newsLetters'=>NewsLetter::all()
        ]);
    }
    public function deleteNewsLetter($id) {
        DB::table('news_letters')->where('id',$id)->delete();
        $notification = array(
            'messege' =>'delete newsLetter done',
            'alert-type' =>'success',
        );
        return redirect()->back()->with($notification);
    }
}
