<?php

namespace App\Http\Controllers\Admin\brand;

use App\Brand;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrandController extends Controller
{
    public function brand() {
        return view('admin.brand.brand',[
            'brands'=>Brand::all()
        ]);
    }
    public function storeBrand(Request $request) {
        $brand= Brand::storeBrand($request);
        $notification=array(
            'messege'=>'Successfully Brand Inserted ',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
    public function deleteBrand($id) {
        $data= DB::table('brands')->where('id',$id)->first();
        $image = $data->brand_logo;
        unlink($image);
        $brand=DB::table('brands')->where('id',$id)->delete();
        $notification=array(
            'messege'=>'Brand Delete Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
   public function editBrand($id) {
        return view('admin.brand.edit_brand',[
            'brand'=>Brand::where('id',$id)->first()
        ]);
   }
   public function updateBrand(Request $request, $id) {
        $updateBrand =Brand::updateBrand($request);
        $notification = array(
           'messege' => 'Update Brand Successfully.',
           'alert-type' => 'success'
       );
       return redirect()->route('brands')->with($notification);
   }
}
