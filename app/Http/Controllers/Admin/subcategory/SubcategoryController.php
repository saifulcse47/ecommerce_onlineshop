<?php

namespace App\Http\Controllers\Admin\subcategory;

use App\Category;
use App\Http\Controllers\Controller;
use App\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubcategoryController extends Controller
{
    public function subCategory() {
        return view('admin.subcategory.subcategory',[
            'subCategories' => Subcategory::leftjoin('categories','categories.id','=','subcategories.category_id')
                ->get([
                    'subcategories.id',
                    'subcategories.subcategory_name',
                    'categories.id as category_id',
                    'categories.category_name',
                ]),
            'categories' =>Category::all()

        ]);
    }
    public function storeSubCategory(Request $request) {
        Subcategory::storeSubCategory($request);
        $notification = array(
            'messege' => 'Insert SubCategory Successfully.',
            'alert-type' => 'success'
        );
        return redirect()->route('subcategories')->with($notification);
    }
    public function editSubCategory($id) {
        return view('admin.subcategory.edit-subcategory',[
            'subcategory'=>Subcategory::where('id',$id)->first(),
            'categories' =>Category::all()
        ]);
    }
    public function updateSubCategory(Request $request,$id,$notification) {
        Subcategory::updateSubCategory($request,$notification);
        return redirect()->route('categories')->with($notification);
    }
    public function deleteSubCategory($id) {
       DB::table('subcategories')->where('id',$id)->delete();
        $notification = array(
            'messege' => 'Delete Subcategory done',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
    }
}
