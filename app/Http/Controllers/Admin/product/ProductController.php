<?php

namespace App\Http\Controllers\Admin\product;

use App\Brand;
use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Subcategory;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function addProduct() {
        return view('admin.product.add-product',[
            'categories'    =>Category::all(),
            'subCategories' =>Subcategory::all(),
            'brands'        =>Brand::all()
        ]);
    }
    public function allProduct() {
        $products=DB::table('products')
                    ->join('categories','categories.id','=','products.category_id')
                    ->join('brands','products.brand_id','=','brands.id')
                    ->select('products.*','categories.category_name','brands.brand_name','brands.brand_logo')
                    ->get();
       return view('admin.product.all-product',compact('products'));
    }

    public function imageUpload($imageOne){
        $imageOneName=hexdec(uniqid()).'.'.$imageOne->getClientOriginalExtension();
        Image::make($imageOne)->resize(230,300);
        $directory = 'media/product/';
        $imageOne->move($directory,$imageOneName);
        return $directory.$imageOneName;
    }
    public function productImageTwo($imageTwo) {
        $imageTwoName=hexdec(uniqid()).'.'.$imageTwo->getClientOriginalExtension();
        Image::make($imageTwo)->resize(230,300);
        $directory = 'media/product/';
        $imageTwo->move($directory,$imageTwoName);
        return $directory.$imageTwoName;
    }
    public function productImageThree($imageThree) {
        $imageThreeName=hexdec(uniqid()).'.'.$imageThree->getClientOriginalExtension();
        Image::make($imageThree)->resize(230,300);
        $directory = 'media/product/';
        $imageThree->move($directory,$imageThreeName);
        return $directory.$imageThreeName;
    }
    public function storeProduct(Request $request) {
        $data= array();
        $data['product_name']           =$request->product_name;
        $data['product_code']           =$request->product_code;
        $data['product_quantity']       =$request->product_quantity;
        $data['category_id']            =$request->category_id;
        $data['sub_category_id']        =$request->sub_category_id;
        $data['brand_id']               =$request->brand_id;
        $data['product_size']           =$request->product_size;
        $data['product_colors']         =$request->product_colors;
        $data['selling_price']          =$request->selling_price;
        $data['product_details']        =$request->product_details;
        $data['video_link']             =$request->video_link;
        $data['main_slider']            =$request->main_slider;
        $data['hot_deal']               =$request->hot_deal;
        $data['best_rated']             =$request->best_rated;
        $data['mid_slider']             =$request->mid_slider;
        $data['hot_new']                =$request->hot_new;
        $data['trend']                  =$request->trend;
        $data['status'] =1;

        $imageOne=$request->image_one;
        $imageTwo=$request->image_two;
        $imageThree=$request->image_three;
        if ($imageOne && $imageTwo && $imageThree) {
            $data['image_one']=$this->imageUpload($request->file('image_one'));
            $data['image_two']=$this->productImageTwo($request->file('image_two'));
            $data['image_three']=$this->productImageThree($request->file('image_three'));

            $product=DB::table('products')
                ->insert($data);
            $notification=array(
                'messege'=>'Successfully Product Inserted ',
                'alert-type'=>'success'
            );
            return Redirect()->back()->with($notification);
        }
    }
    public function inactiveProduct($id) {
        DB::table('products')->where('id',$id)->update(['status'=>0]);
        $notification=array(
            'messege'=>'Active Product Successfully',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
    public function activeProduct($id) {
        DB::table('products')->where('id',$id)->update(['status'=>1]);
        $notification=array(
            'messege'=>'Inactive Product Successfully',
            'alert-type'=>'success'
        );
    }
    public function editProduct($id) {
//        $product = DB::table('products')->where('id',$id)->first();
//        $brands = DB::table('brands')->get();
        return view('admin.product.edit-product',[
            'product'           => Product::where('id',$id)->first(),
            'categories'        =>Category::all(),
            'subCategory'       =>Subcategory::all(),
            'brands'            =>Brand::all()
        ]);
    }
    public function updateProductWithoutPhoto(Request $request,$id) {
        $data= array();
        $data['product_name']           =$request->product_name;
        $data['product_code']           =$request->product_code;
        $data['product_quantity']       =$request->product_quantity;
        $data['category_id']            =$request->category_id;
        $data['sub_category_id']        =$request->sub_category_id;
        $data['brand_id']               =$request->brand_id;
        $data['product_size']           =$request->product_size;
        $data['product_colors']         =$request->product_colors;
        $data['selling_price']          =$request->selling_price;
        $data['discount_price']         =$request->discount_price;
        $data['product_details']        =$request->product_details;
        $data['video_link']             =$request->video_link;
        $data['main_slider']            =$request->main_slider;
        $data['hot_deal']               =$request->hot_deal;
        $data['best_rated']             =$request->best_rated;
        $data['mid_slider']             =$request->mid_slider;
        $data['hot_new']                =$request->hot_new;
        $data['trend']                  =$request->trend;

        $update=DB::table('products')->where('id',$id)->update($data);
        if ($update){
            $notification=array(
                'messege'=>'Successfully Product Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->route('admin.all-product')->with($notification);
        }
        else{
            $notification=array(
                'messege'=>'Nothing to Update',
                'alert-type'=>'success'
            );
            return Redirect()->route('admin.all-product')->with($notification);
        }
    }
    //uploadProductPhoto
    public function updateProductPhoto(Request $request,$id) {
        $imageOne       =$request->image_one;
        $imageTwo       =$request->image_two;
        $imageThree     =$request->image_three;

        $OldImageOne    =$request->old_one;
        $OldImageTwo    =$request->old_two;
        $OldImageThree  =$request->old_three;
        $data           =array();
        if ($request->has('image_one')) {
            unlink($OldImageOne);
            $data['image_one']=$this->imageUpload($request->file('image_one'));
            DB::table('products')->where('id',$id)->update($data);
            $notification=array(
                'messege'=>'Product Photo one Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->route('admin.all-product')->with($notification);
        }
        if ($request->has('image_two')) {
            unlink($OldImageTwo);
            $data['image_two']=$this->productImageTwo($request->file('image_two'));
            DB::table('products')->where('id',$id)->update($data);
            $notification=array(
                'messege'=>'Product Photo Two Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->route('admin.all-product')->with($notification);
        }
        if ($request->has('image_three')){
            unlink($OldImageThree);
            $data['image_three']=$this->productImageThree($request->file('image_three'));
            DB::table('products')->where('id',$id)->update($data);
            $notification=array(
                'messege'=>'Product Photo Three Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->route('admin.all-product')->with($notification);
        }
        if ($request->has('image_two') && $request->has('image_three')) {
            unlink($OldImageTwo);
            unlink($OldImageThree);
            $data['image_two']=$this->productImageTwo($request->file('image_two'));
            $data['image_three']=$this->productImageThree($request->file('image_three'));
            DB::table('products')->where('id',$id)->update($data);
            $notification=array(
                'messege'=>'Photo one & Three Updated ',
                'alert-type'=>'success'
            );
            return Redirect()->route('admin.all-product')->with($notification);
        }
        if ($request->has('image_one') && $request->has('image_two') && $request->has('image_three')) {
            echo 'one,two,three';
        }
        else{
            echo 'blank';
        }
    }
    public function deleteProduct($id) {
      $product =  DB::table('products')->where('id',$id)->first();
      $imageOne =$product->image_one;
      $imageTwo =$product->image_two;
      $imageThree =$product->image_three;
      if ($imageOne){
          unlink($imageOne);
      }
      elseif ($imageOne && $imageTwo && $imageThree){
          unlink($imageOne);
          unlink($imageTwo);
          unlink($imageThree);
      }
      DB::table('products')->where('id',$id)->delete();
        $notification=array(
            'messege'=>'Successfully Product Deleted ',
            'alert-type'=>'success'
        );
        return Redirect()->back()->with($notification);
    }
    public function viewProduct($id) {
        $product = DB::table('products')
                    ->join('categories','categories.id','=','products.category_id')
                    ->join('brands','products.brand_id','=','brands.id')
                    ->join('subcategories','subcategories.id','=','products.sub_category_id')
                    ->select('products.*',
                        'categories.category_name',
                        'brands.brand_name',
                        'brands.brand_logo',
                        'subcategories.subcategory_name')
                    ->where('products.id',$id)
                    ->first();
        return view('admin.product.view-product',compact('product'));
    }

//    subcategory collect by ajax request
    public function getSubCategory($category_id) {
       $cat= DB::table('subcategories')->where('category_id',$category_id)->get();
       return json_encode($cat);
    }
}
