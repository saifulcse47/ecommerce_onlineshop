<?php

namespace App\Http\Controllers\Admin\post;

use App\Category;
use App\Http\Controllers\Controller;
use App\Post;
use App\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Image;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createPostCategory() {
        return view('admin.post.post-category');
    }
    public function storePostCategory(Request $request) {
        PostCategory::savePostCategory($request);
        $notification = array(
            'messege' =>'Successfully Create Post Category',
            'alert-type' =>'success',
        );
        return redirect()->back()->with($notification);
    }
    public function allPostCategory() {
        return view('admin.post.all-post-category',[
            'postCategories'=> PostCategory::all()
        ]);
    }
    public function editPostCategory($id) {
        return view('admin.post.edit-post-category',[
           'postCategory'=> PostCategory::find($id)
        ]);
    }
    public function updatePostCategory(Request $request) {
        $postCat=PostCategory::UpdatePostCategoryName($request);
        if ($postCat) {
            $notification = array(
                'messege' =>'Successfully Updated Post Category',
                'alert-type' =>'success',
            );
        }
        else{
            $notification = array(
                'messege' =>'Nothing to Update Post Category',
                'alert-type' =>'success',
            );
        }
        return redirect()->route('admin.all.post.category')->with($notification);
    }
    public function deletePostCategory($id) {
        $postCat = DB::table('post_categories')->where('id',$id)->delete();
        $notification = array(
            'messege' =>'delete Post Category done',
            'alert-type' =>'success',
        );
        return redirect()->back()->with($notification);
    }
    public function createPostBlog() {
        return view('admin.blog.create-blog',[
            'postCategories'=>PostCategory::all()
        ]);
    }
    public function storePostBlog(Request $request) {
        Post::saveBlogPost($request);
        $notification = array(
            'messege' =>'Save Blog Post Success',
            'alert-type' =>'success',
        );
        return redirect()->back()->with($notification);
    }
    public function managePostBlog() {
        $blogPosts=DB::table('posts')
            ->join('post_categories','post_categories.id','=','posts.category_id')
            ->select('posts.*','post_categories.category_name_en')
            ->get();
        return view('admin.blog.manage-blog-post',compact('blogPosts'));
    }
    public function deletePostBlog($id) {
        $data=DB::table('posts')->where('id',$id)->first();
        $image=$data->post_image;
        unlink($image);
        DB::table('posts')->where('id',$id)->delete();
        $notification=array(
            'messege'=>'Delete Post Blog done.',
            'alert-type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function editPostBlog($id) {
        return view('admin.blog.edit-post-blog',[
            'postBlog'=>Post::where('id',$id)->first(),
            'postCategories'=>PostCategory::all()
        ]);
    }
    public function updatePostBlog(Request $request) {
        $postBlog=Post::updateBlogPost($request);
        if ($postBlog) {
            $notification=array(
                'messege'=>'Update Success Post Blog done.',
                'alert-type'=>'success'
            );
        }else{
            $notification=array(
                'messege'=>'Nothing to Update Post Blog.',
                'alert-type'=>'success'
            );
        }
        return redirect()->route('all.post.blog')->with($notification);
    }
}
