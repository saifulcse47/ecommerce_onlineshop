<?php

namespace App\Http\Controllers\Admin\Category;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Array_;

class CategoryController extends Controller
{
    public function category() {
        return view('admin.category.category',[
            'categories'=>Category::all()
        ]);
    }
    public function storeCategory(Request $request) {
        $validatedData = $request->validate([
            'category_name' => 'required|unique:categories|max:55',
        ]);
            Category::storeCategory($request);
//            return redirect('admin/store/category')->with('message','Save Category Successful');
        $notification=array(
            'messege'=>'Category Save done.',
            'alert-type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function deleteCategory($id) {
        DB::table('categories')->where('id',$id)->delete();
        $notification=array(
            'messege'=>'Delete Category done.',
            'alert-type'=>'success'
        );
        return redirect()->back()->with($notification);
    }
    public function editCategory($id) {
//       $category= DB::table('categories')->where('id',$id)->first();
       return view('admin.category.edit-category',[
          'category'=> Category::where('id',$id)->first()
       ]);
    }
    public function updateCategory(Request $request,$id)
    {
        Category::updateCategory($request);
        $data=Array();
        $data['category_name'] = $request->category_name;
        $UpdateCategory= DB::table('categories')->where('id',$id)->update($data);
        if ($UpdateCategory) {
            $notification = array(
                'messege' => 'Update Category Successfully.',
                'alert-type' => 'success'
            );
            return redirect()->route('categories')->with($notification);
        } else {
            $notification = array(
                'messege' => 'Nothing to update.',
                'alert-type' => 'success'
            );
            return redirect()->route('categories')->with($notification);
        }
    }
}
