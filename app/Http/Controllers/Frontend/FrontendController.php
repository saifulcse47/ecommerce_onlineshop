<?php

namespace App\Http\Controllers\Frontend;

use App\Category;
use App\Http\Controllers\Controller;
use App\NewsLetter;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FrontendController extends Controller
{
    public function index() {
        $cats=Category::take(3)->first();
        $categoryId=$cats->id;
        $productCats=Product::where('category_id',$categoryId)
            ->where('status',1)
            ->limit(16)
            ->orderBy('id','DESC')
            ->get();
        return view('frontend.home.home',compact('cats','productCats'),
            [
            'categories'    =>Category::all(),
            'product'       =>Product::leftjoin('brands','brands.id','products.brand_id')->select('products.*','brands.brand_name')->where('main_slider',1)->orderBy('id','DESC')->first(),
            'features'      =>Product::where('status',1)->orderBy('id','DESC')->limit(24)->get(),
            'trends'        =>Product::where('status',1)->where('trend',1)->orderBy('id','DESC')->limit(24)->get(),
            'bestRates'     =>Product::where('status',1)->where('best_rated',1)->orderBy('id','DESC')->limit(24)->get(),
            'dealsWeeks'     =>Product::leftjoin('brands','brands.id','products.brand_id')
                            ->select('products.*','brands.brand_name')
                            ->where('products.status',1)
                            ->where('hot_deal',1)
                            ->orderBy('id','DESC')
                            ->limit(4)
                            ->get(),
            'midSliders'     =>Product::join('brands','brands.id','products.brand_id')
                            ->join('categories','categories.id','products.category_id')
                            ->select('products.*','brands.brand_name','categories.category_name')
                            ->where('products.status',1)
                            ->where('mid_slider',1)
                            ->orderBy('id','DESC')
                            ->limit(4)
                            ->get(),
        ]);
    }

    public function newsLetter(Request $request) {
        NewsLetter::newsLetterEmail($request);
        $notification = array(
            'messege' => 'Thanks for subscribe',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notification);
//        return redirect('/')->with('message','Thanks for subscribe');
    }
}
