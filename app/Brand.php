<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable=['brand_name','brand_logo'];
    public static function storeBrand($request) {
        $validatedData = $request->validate([
            'brand_name' => 'required|unique:brands|max:55',
            'brand_logo' => 'required',
        ]);
        $image = $request->file('brand_logo');
        $imageName=date('dmy_H_s_i');
        $ext=strtolower($image->getClientOriginalExtension());
        $imageFullName=$imageName.'.'.$ext;
        $uploadPath='public/media/brand/';
        $imageUrl = $uploadPath.$imageName;
        $success=$image->move($uploadPath,$imageName);
        $brand = new Brand();
        $brand->brand_name = $request->brand_name;
        $brand->brand_logo =$uploadPath.$imageName;
        $brand->save();
    }
    public static function updateBrand($request) {
        $brand = Brand::find($request->id);
        $brandImage = $request->file('brand_logo');
        if ($brandImage){
            unlink($brand->brand_logo);
            $imageName=date('dmy_H_s_i');
            $ext=strtolower($brandImage->getClientOriginalExtension());
            $imageFullName=$imageName.'.'.$ext;
            $uploadPath='public/media/brand/';
            $imageUrl = $uploadPath.$imageName;
            $success=$brandImage->move($uploadPath,$imageName);

            $brand->brand_name = $request->brand_name;
            $brand->brand_logo =$uploadPath.$imageName;
            $brand->save();
        }
        else{
            $brand->brand_name = $request->brand_name;
            $brand->save();
        }
    }

}
