<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostCategory extends Model
{
    protected $fillable=['category_name_en','category_name_bn'];

    public static function savePostCategory($request) {
        $post                       = new PostCategory();
        $post->category_name_en     =$request->category_name_en;
        $post->category_name_bn     =$request->category_name_bn;
        $post->save();
    }
    public static function UpdatePostCategoryName($request) {
        $postCategory = PostCategory::find($request->id);
        $postCategory->category_name_en     =$request->category_name_en;
        $postCategory->category_name_bn     =$request->category_name_bn;
        $postCategory->save();
    }
}
