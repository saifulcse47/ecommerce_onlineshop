<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use MongoDB\Driver\Session;

class Sign extends Model
{
    protected $fillable =['user_name','user_email','user_phone','user_password','gender','user_address','avatar'];
    public static function saveUser($request){
        $user = new Sign();
        $user->user_name =$request->user_name;
        $user->user_email =$request->user_email;
        $user->user_phone =$request->user_phone;
        $user->user_password =bcrypt($request->user_password);
        $user->gender =$request->gender;
        $user->user_address =$request->user_address;
        $user->save();
        Session::put('userId',$user->id);
        Session::put('userName',$user->user_name);
    }
}
