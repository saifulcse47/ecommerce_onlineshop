<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Image;

class Product extends Model
{
    protected $fillable=[
        'category_id',
        'sub_category_id',
        'brand_id',
        'product_name',
        'product_code',
        'product_quantity',
        'product_details',
        'product_colors',
        'product_size',
        'selling_price',
        'discount_price',
        'video_link',
        'main_slider',
        'hot_deal',
        'best_rated',
        'mid_slider',
        'hot_new',
        'trend',
        'image_one',
        'image_two',
        'image_three',
        'status',
        ];

    public static function saveProduct($request) {
        $validatedData = $request->validate([
            'product_name' => 'required|unique:products|max:55',
            'product_code' => 'required',
            'selling_price' => 'required',
            'image_one' => 'required',
            'image_two' => 'required',
            'image_three' => 'required',
            'product_details' => 'required|max:250',
        ]);
            $product        = new Product();

            $image_one      = $request->file('image_one');
            $image_two      = $request->file('image_two');
            $image_three    = $request->file('image_three');


            $image_one_name =hexdec(uniqid()).'.'.$image_one->getClientOriginalExtension();
            $imagePathOne=Image::make($image_one)->resize(300,300)->save('public/media/product'.$image_one_name);
            $product->image_one ='public/media/product'.$image_one_name;

            $image_two_name =hexdec(uniqid()).'.'.$image_two->getClientOriginalExtension();
            $imagePathTwo=Image::make($image_two)->resize(300,300)->save('public/media/product'.$image_two_name);
            $product->image_two  ='public/media/product'.$image_two_name;

            $image_three_name =hexdec(uniqid()).'.'.$image_three->getClientOriginalExtension();
            $imagePathThree=Image::make($image_three)->resize(300,300)->save('public/media/product'.$image_three_name);
            $product->image_three   ='public/media/product'.$image_three_name;

            $product->product_name          =$request->product_name;
            $product->product_code          =$request->product_code;
            $product->product_quantity      =$request->product_quantity;
            $product->category_id           =$request->category_id;
            $product->sub_category_id       =$request->sub_category_id;
            $product->brand_id              =$request->brand_id;
            $product->product_size          =$request->product_size;
            $product->product_colors        =$request->product_colors;
            $product->selling_price         =$request->selling_price;
            $product->product_details       =$request->product_details;
            $product->video_link            =$request->video_link;
            $product->main_slider           =$request->main_slider;
            $product->hot_deal              =$request->hot_deal;
            $product->best_rated            =$request->best_rated;
            $product->mid_slider            =$request->mid_slider;
            $product->hot_new               =$request->hot_new;
            $product->trend                 =$request->trend;
            $product->status = 1;
            $product->save();
    }
}
