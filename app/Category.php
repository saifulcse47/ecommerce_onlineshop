<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['category_name'];
    public static function storeCategory($request) {
        $category = new Category();
        $category->category_name    =  $request->category_name;
        $category->save();
    }
    public static function updateCategory($request) {
        $validatedData = $request->validate([
            'category_name' => 'required|max:55',
        ]);
//        $category = Category::find($request->id);
//        $category->category_name    =   $request->category_name;
//        $category->save();

    }
}
