<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable =['coupon_code','coupon_discount'];

    public static function storeCoupon($request) {
        $validatedData = $request->validate([
            'coupon_code' => 'required|max:55',
            'coupon_discount' => 'required',
        ]);
        $coupon = new Coupon();
        $coupon->coupon_code        =$request->coupon_code;
        $coupon->coupon_discount    =$request->coupon_discount;
        $coupon->save();
    }
    public static function updateCoupon ($request) {

        $coupon = Coupon::find($request->id);
        $coupon->coupon_code               =$request->coupon_code;
        $coupon->coupon_discount           =$request->coupon_discount;
        $coupon->save();
        //another way
    }
}
