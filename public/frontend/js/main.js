function userFormValidation() {
    var name                =  document.getElementById('user_name').value;
    var lastName            =  document.getElementById('last_name').value;
    var email               =  document.getElementById('user_email').value;
    var phone               =  document.getElementById('user_phone').value;
    var password            =  document.getElementById('user_password').value;
    var confirm_password    =  document.getElementById('confirm_password').value;
    var gender              =  document.getElementById('gender').value;
    var checkBox            =  document.getElementById('check_box').value;

    if (name == ""){
        document.getElementById('name_error').innerHTML="Enter your name";
        return false;
    }
    if (lastName == ""){
        document.getElementById('last_name_error').innerHTML="Enter your last name";
        return false;
    }
    if (email== ""){
        document.getElementById('email_error').innerHTML="Enter your email address";
        return false;
    }
    if (phone == ""){
        document.getElementById('phone_error').innerHTML="Enter your phone number";
        return false;
    }
    if (password.length<8 ){
        document.getElementById('password_error').innerHTML="Password to short"
    }
    if (confirm_password != password ){
        document.getElementById('confirm_password_error').innerHTML="Password not match"
    }
    if (gender=="" ){
        document.getElementById('gender_error').innerHTML="select your gender"
    }
    if (checkBox=="1" || checkBox=="0" ){
        document.getElementById('gender_error').innerHTML=""
    }
    else {
        document.getElementById('check_box_error').innerHTML="If you not agree you can't access next step"
    }
}

function valid() {
    var name = document.getElementById("nm1").value;
    var em = document.getElementById("em1").value;
    var pass = document.getElementById("ps1").value;
    var cpass = document.getElementById("cps1").value;

    if (name == "") {

        document.getElementById("nm_err").innerHTML="Enter your name";
        return false;
    }

    if (em == "") {

        document.getElementById("em_err").innerHTML="Enter your Email";
        return false;
    }
    if (pass == "") {

        document.getElementById("ps1_err").innerHTML="Enter your Pass";
        return false;
    }
    if(pass.length<5){
        document.getElementById("ps1_err").innerHTML="too short ";
        return false;
    }
    else{
        document.getElementById("ps1_err").innerHTML="";
    }
    if(cpass != pass){
        document.getElementById("cps1_err").innerHTML="Not Match";
        return false;
    }




}

